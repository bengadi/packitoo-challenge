class RattesController < ApplicationController
  def create
    @article = Article.find(params[:article_id])
    @ratte = @article.comments.create(ratte_params)
    redirect_to article_path(@article)
  end
 
  private
    def ratte_params
      params.require(:ratte).permit(:score)
    end
end