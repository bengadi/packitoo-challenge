class RatingsController < ApplicationController
  def create
    @article = Article.find(params[:article_id])
    @rating = @article.ratings.create(rating_params)
    redirect_to article_path(@article)
  end
   def index
    @rating = rating.all
    respond_to do|format|
    format.html
    format.csv{ render text: @articles.to_csv}
  end
  end
  def new
  @rating = Rating.new
  end
  private
    def rating_params
      params.require(:rating).permit(:score)
    end
end
