require 'csv'
class Article < ActiveRecord::Base
  has_many :ratings, dependent: :destroy
  has_many :comments, dependent: :destroy
  validates :title, presence: true,length: { minimum: 5 }
  validates :text, presence: true,length: { minimum: 5 }

                     
 def self.to_csv
 	attributes=%w{title lowest-rating highest-rating average-score number-of-comments average-number-of-comments}
 	CSV.generate do |csv|
 		csv << attributes
 		all.each do |article|
 			a=article.title
 			b=article.ratings.minimum(:score)
 			c=article.ratings.maximum(:score)
 			d=article.ratings.average(:score)
 			f=article.comments.count
 			h=0
 			article.comments.each do |comment|
 				b=comment.body
 				h+=b.size
 			end
 			if f==0
 				e=0
 			else
 				e=h.fdiv(f)
 			end
 		    csv << [a,b,c,d,f,e]

 	 end
  end
 end 
                
end
