require 'test_helper'

class RatingTest < ActiveSupport::TestCase
test "the average rating of 2 and 5 should be 3.5" do
a=Article.new
a.title='dfghfgdg dfgjklrtyuivbn'
a.text='azqfkdfgfdgdfg sldfjkq sdfmdkhdk'

rating1=Rating.new
rating1.score=2
a.ratings<<rating1
rating2=Rating.new
rating2.score=5
a.ratings<<rating2
rating2.save
rating1.save
a.save
assert_equal( 3.5, a.ratings.average(:score), "the average rating of 2 and 5 shoud be 3.5" )
end
end
