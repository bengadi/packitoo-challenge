class CreateRatings < ActiveRecord::Migration
  def up
  def change
    create_table :ratings do |t|
      t.integer :score
      t.references :article, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
end